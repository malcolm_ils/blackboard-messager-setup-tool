﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILS_Blackboard_Messages
{
    public class Institution
    {
        public string Name { get; set; }
        public string ID { get; set; }

       public List<Group> GroupList { get; set; }
    }
}
