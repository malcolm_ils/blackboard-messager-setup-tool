﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILS_Blackboard_Messages
{
    public class MessageTemplate
    {
        private int _id;
        private string _name;
        private string _content;


        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public MessageTemplate(int id, string name, string content)
        {
            ID = id;
            Name = name;
            Content = content;
        }
    }
}
