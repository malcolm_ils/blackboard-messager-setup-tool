﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILS_Blackboard_Messages
{
    public class Group
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string InstName { get; set; }
        public string InstID { get; set; }
        public string Type { get; set; }
    }
}
