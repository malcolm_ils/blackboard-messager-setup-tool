﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace ILS_Blackboard_Messages
{
    public partial class Form1 : Form
    {
        public static NotificationServiceClient Client;
        public static string Key;
        public static string Secret;
        public static List<MessageTemplate> templates = new List<MessageTemplate>();
        public static List<Group> groups = new List<Group>();
        public static List<Institution> schools = new List<Institution>();

        public static CurrentData currentData = new CurrentData();
        //public static List<object> templates;
        public Form1()
        {

            InitializeComponent();

            //Key_TextBox.Text = "1557Fq5q4";
            //Secret_TextBox.Text = "vxeFfRxvDgWc2dS";
        }

        private void Submit_Button_Click(object sender, EventArgs e)
        {
            bool isValid = true;
            argumentsBtn.Enabled = false;
            errorProvider1.Clear();
            //Start URL reguest
            Key = Key_TextBox.Text;
            Secret = Secret_TextBox.Text;

            Client = new NotificationServiceClient();

            if(Key == null || Key.Length <= 0)
            {
                errorProvider1.SetError(Key_TextBox, "API Key required");
                isValid = false;
            }
            if(Secret == null || Secret.Length <= 0)
            {
                errorProvider1.SetError(Secret_TextBox, "API secret required");
                isValid = false;
            }

            if (isValid)
            {
                getTemplates(Key, Secret);
            }
        }

        private void getTemplates(string user, string pass)
        {
            Client.ClientCredentials.UserName.UserName = user;
            Client.ClientCredentials.UserName.Password = pass;
            currentData.Key = user;
            currentData.Secret = pass;
            try 
            {
                processTemplates(Client.GetAllTemplates());
                processGroups(Client.GetAllGroups());
                argumentsBtn.Enabled = true;
            } 
            catch(Exception e) {
                errorProvider1.SetError(Submit_Button, "Error: " + e.InnerException.Message);
            }
            
            //Console.WriteLine(Client.GetAllGroups());
            //processTemplates(xml);
        }
        private void processGroups(string xml)
        {
            XmlDocument doc = new XmlDocument();
            Group temp;

            if(groups != null){ groups.Clear(); }
            if(schools != null) { schools.Clear(); }
            
            doc.LoadXml("<xml>" + xml + "</xml>");
            XmlNode node = doc.DocumentElement.SelectSingleNode("Groups");
            foreach (XmlNode group in node)
            {
                temp = new Group();
                temp.ID = group.SelectSingleNode("Id").InnerText;
                temp.Name = group.SelectSingleNode("Name").InnerText;
                temp.InstName = group.SelectSingleNode("InstName").InnerText;
                temp.InstID = group.SelectSingleNode("InstId").InnerText;
                groups.Add(temp);
            }
            schools = new List<Institution>();
            List<string> Ids = new List<string>(groups.Select(x => x.InstID).Distinct());
            
            Ids.ForEach(id =>
            {
                schools.Add(new Institution
                {
                    ID = id,
                    Name = groups.Where(x => x.InstID == id).First<Group>().InstName,
                    GroupList = groups.Where<Group>(x => x.InstID == id).ToList<Group>(),
                });

            });
            
            Console.WriteLine(schools.Count);
            Console.WriteLine(schools[0].GroupList.First().Name);

            InstitutionDropDown.DisplayMember = "Name";
            InstitutionDropDown.DataSource = schools;
        }




        private void processTemplates(string xml)
        {
            XmlDocument doc = new XmlDocument();
            MessageTemplate temp;
            int id;
            string name, content;
            if (templates != null) {
                templates.Clear();
            }
            //templates = new List<MessageBox>();
            doc.LoadXml("<xml>" + xml + "</xml>");
            XmlNode node = doc.DocumentElement.SelectSingleNode("Templates");
            foreach(XmlNode template in node)
            {
                id = int.Parse(template.SelectSingleNode("Id").InnerText);
                name = template.SelectSingleNode("Name").InnerText;
                content = template.SelectSingleNode("Content").InnerText;
                temp = new MessageTemplate(id, name, content);
                templates.Add(temp);
               // Templates_ListBox.Items.Add(temp.ID.ToString());
            }
            Templates_ListBox.DisplayMember = "Name";
            Templates_ListBox.DataSource = templates;
           

            //Console.WriteLine(node.ToString());
            //Console.ReadLine();
        }

        private void Templates_ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            MessageTemplate template = templates[Templates_ListBox.SelectedIndex];
            templateID.Text = template.ID.ToString();
            currentData.TemplateID = template.ID.ToString();

            
            XDocument xDoc = XDocument.Parse(Client.GetTemplateContent(template.ID.ToString()));
            Message_TextBox.Text = xDoc.ToString();   

        }

        private void argumentsBtn_Click(object sender, EventArgs e)
        {
            ArgPreviewForm argForm = new ArgPreviewForm(currentData);
            //argForm.data = currentData;
            argForm.ShowDialog();
            
            //MessageBox.Show(currentData.ToString(), "Current Args");

        }

        private void GroupDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentData.GroupID = groups[GroupDropdown.SelectedIndex].ID;
        }

        private void InstitutionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            groups = schools[InstitutionDropDown.SelectedIndex].GroupList;
            GroupDropdown.DisplayMember = "Name";

            GroupDropdown.DataSource = groups;
        }
    }

    public class CurrentData
    {
        public string TemplateID { get; set; }
        public string Secret { get; set; }
        public string Key { get; set; }
        public string GroupID { get; set; }

        
        public override string ToString()
        {
            string str = "";
            //str += "\nUser:   " + Key;
            //str += "\nPass:   " + Secret;
            //str += "\nTemplateID:   " + TemplateID;
            //str += "\nGroupID:   " + GroupID;
            //str += "\n\nFormatted arg:\n";
            str += Key + " " + Secret + " " + TemplateID+" "+GroupID+" noreply@ilsny.com";
            return str;
        }

    }

}
