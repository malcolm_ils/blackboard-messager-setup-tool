﻿namespace ILS_Blackboard_Messages
{
    partial class ArgPreviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ArgPreviewForm));
            this.args_txtbox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.user_txtbox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.groupId_txtbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.templateId_txtbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pass_txtbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // args_txtbox
            // 
            this.args_txtbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.args_txtbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.args_txtbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.args_txtbox.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.args_txtbox.Location = new System.Drawing.Point(26, 193);
            this.args_txtbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.args_txtbox.Name = "args_txtbox";
            this.args_txtbox.ReadOnly = true;
            this.args_txtbox.Size = new System.Drawing.Size(410, 22);
            this.args_txtbox.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(189, 239);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 28);
            this.button1.TabIndex = 1;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(128, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "User:";
            // 
            // user_txtbox
            // 
            this.user_txtbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.user_txtbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.user_txtbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user_txtbox.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.user_txtbox.Location = new System.Drawing.Point(173, 15);
            this.user_txtbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.user_txtbox.Name = "user_txtbox";
            this.user_txtbox.Size = new System.Drawing.Size(173, 15);
            this.user_txtbox.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.user_txtbox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.groupId_txtbox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.templateId_txtbox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.pass_txtbox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.args_txtbox);
            this.panel1.Location = new System.Drawing.Point(14, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(463, 287);
            this.panel1.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(165, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Formatted Arguments";
            // 
            // groupId_txtbox
            // 
            this.groupId_txtbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.groupId_txtbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.groupId_txtbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupId_txtbox.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupId_txtbox.Location = new System.Drawing.Point(173, 111);
            this.groupId_txtbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupId_txtbox.Name = "groupId_txtbox";
            this.groupId_txtbox.Size = new System.Drawing.Size(173, 15);
            this.groupId_txtbox.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(104, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Group ID:";
            // 
            // templateId_txtbox
            // 
            this.templateId_txtbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.templateId_txtbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.templateId_txtbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateId_txtbox.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.templateId_txtbox.Location = new System.Drawing.Point(173, 79);
            this.templateId_txtbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.templateId_txtbox.Name = "templateId_txtbox";
            this.templateId_txtbox.Size = new System.Drawing.Size(173, 15);
            this.templateId_txtbox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(87, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Template ID:";
            // 
            // pass_txtbox
            // 
            this.pass_txtbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.pass_txtbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pass_txtbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pass_txtbox.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.pass_txtbox.Location = new System.Drawing.Point(173, 47);
            this.pass_txtbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pass_txtbox.Name = "pass_txtbox";
            this.pass_txtbox.Size = new System.Drawing.Size(173, 15);
            this.pass_txtbox.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(125, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Pass:";
            // 
            // ArgPreviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.ClientSize = new System.Drawing.Size(491, 316);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ArgPreviewForm";
            this.ShowIcon = false;
            this.Text = "ArgPreviewForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox args_txtbox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox user_txtbox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox groupId_txtbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox templateId_txtbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox pass_txtbox;
        private System.Windows.Forms.Label label2;
    }
}