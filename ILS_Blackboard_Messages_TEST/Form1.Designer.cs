﻿namespace ILS_Blackboard_Messages
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Key_TextBox = new System.Windows.Forms.TextBox();
            this.Secret_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Submit_Button = new System.Windows.Forms.Button();
            this.Message_TextBox = new System.Windows.Forms.TextBox();
            this.Templates_ListBox = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TemplateList_Lbl = new System.Windows.Forms.Label();
            this.TemplateID_label = new System.Windows.Forms.Label();
            this.templateID = new System.Windows.Forms.Label();
            this.argumentsBtn = new System.Windows.Forms.Button();
            this.GroupDropdown = new System.Windows.Forms.ComboBox();
            this.InstitutionDropDown = new System.Windows.Forms.ComboBox();
            this.Schools_Lbl = new System.Windows.Forms.Label();
            this.Group_Lbls = new System.Windows.Forms.Label();
            this.messageTemplateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.messageTemplateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // Key_TextBox
            // 
            this.Key_TextBox.BackColor = System.Drawing.Color.Gainsboro;
            this.Key_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Key_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Key_TextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.Key_TextBox.Location = new System.Drawing.Point(54, 45);
            this.Key_TextBox.Name = "Key_TextBox";
            this.Key_TextBox.Size = new System.Drawing.Size(208, 23);
            this.Key_TextBox.TabIndex = 0;
            // 
            // Secret_TextBox
            // 
            this.Secret_TextBox.BackColor = System.Drawing.Color.Gainsboro;
            this.Secret_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Secret_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Secret_TextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.Secret_TextBox.Location = new System.Drawing.Point(54, 111);
            this.Secret_TextBox.Name = "Secret_TextBox";
            this.Secret_TextBox.Size = new System.Drawing.Size(208, 23);
            this.Secret_TextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(53, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "API Secret";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(53, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "API Key";
            // 
            // Submit_Button
            // 
            this.Submit_Button.BackColor = System.Drawing.Color.Transparent;
            this.Submit_Button.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Submit_Button.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Submit_Button.Location = new System.Drawing.Point(118, 140);
            this.Submit_Button.Name = "Submit_Button";
            this.Submit_Button.Size = new System.Drawing.Size(81, 33);
            this.Submit_Button.TabIndex = 3;
            this.Submit_Button.Text = "Submit";
            this.Submit_Button.UseVisualStyleBackColor = false;
            this.Submit_Button.Click += new System.EventHandler(this.Submit_Button_Click);
            // 
            // Message_TextBox
            // 
            this.Message_TextBox.BackColor = System.Drawing.Color.Gainsboro;
            this.Message_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Message_TextBox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Message_TextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.Message_TextBox.Location = new System.Drawing.Point(288, 78);
            this.Message_TextBox.Multiline = true;
            this.Message_TextBox.Name = "Message_TextBox";
            this.Message_TextBox.ReadOnly = true;
            this.Message_TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Message_TextBox.Size = new System.Drawing.Size(325, 311);
            this.Message_TextBox.TabIndex = 6;
            this.Message_TextBox.TabStop = false;
            // 
            // Templates_ListBox
            // 
            this.Templates_ListBox.BackColor = System.Drawing.Color.Gainsboro;
            this.Templates_ListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Templates_ListBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Templates_ListBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.Templates_ListBox.FormattingEnabled = true;
            this.Templates_ListBox.ItemHeight = 18;
            this.Templates_ListBox.Location = new System.Drawing.Point(54, 210);
            this.Templates_ListBox.Name = "Templates_ListBox";
            this.Templates_ListBox.Size = new System.Drawing.Size(208, 146);
            this.Templates_ListBox.TabIndex = 7;
            this.Templates_ListBox.SelectedIndexChanged += new System.EventHandler(this.Templates_ListBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(288, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(226, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Raw Message Content Preview";
            // 
            // TemplateList_Lbl
            // 
            this.TemplateList_Lbl.AutoSize = true;
            this.TemplateList_Lbl.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TemplateList_Lbl.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.TemplateList_Lbl.Location = new System.Drawing.Point(53, 187);
            this.TemplateList_Lbl.Name = "TemplateList_Lbl";
            this.TemplateList_Lbl.Size = new System.Drawing.Size(100, 18);
            this.TemplateList_Lbl.TabIndex = 9;
            this.TemplateList_Lbl.Text = "Template List";
            // 
            // TemplateID_label
            // 
            this.TemplateID_label.AutoSize = true;
            this.TemplateID_label.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TemplateID_label.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.TemplateID_label.Location = new System.Drawing.Point(288, 22);
            this.TemplateID_label.Name = "TemplateID_label";
            this.TemplateID_label.Size = new System.Drawing.Size(90, 18);
            this.TemplateID_label.TabIndex = 10;
            this.TemplateID_label.Text = "Template ID";
            // 
            // templateID
            // 
            this.templateID.AutoSize = true;
            this.templateID.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateID.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.templateID.Location = new System.Drawing.Point(399, 22);
            this.templateID.Name = "templateID";
            this.templateID.Size = new System.Drawing.Size(13, 18);
            this.templateID.TabIndex = 11;
            this.templateID.Text = "-";
            // 
            // argumentsBtn
            // 
            this.argumentsBtn.BackColor = System.Drawing.Color.Transparent;
            this.argumentsBtn.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.argumentsBtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.argumentsBtn.Location = new System.Drawing.Point(336, 423);
            this.argumentsBtn.Name = "argumentsBtn";
            this.argumentsBtn.Size = new System.Drawing.Size(237, 43);
            this.argumentsBtn.TabIndex = 12;
            this.argumentsBtn.Text = "Show Args";
            this.argumentsBtn.UseVisualStyleBackColor = false;
            this.argumentsBtn.Click += new System.EventHandler(this.argumentsBtn_Click);
            // 
            // GroupDropdown
            // 
            this.GroupDropdown.BackColor = System.Drawing.Color.Gainsboro;
            this.GroupDropdown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GroupDropdown.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupDropdown.FormattingEnabled = true;
            this.GroupDropdown.Location = new System.Drawing.Point(54, 453);
            this.GroupDropdown.Name = "GroupDropdown";
            this.GroupDropdown.Size = new System.Drawing.Size(208, 24);
            this.GroupDropdown.TabIndex = 13;
            this.GroupDropdown.SelectedIndexChanged += new System.EventHandler(this.GroupDropdown_SelectedIndexChanged);
            // 
            // InstitutionDropDown
            // 
            this.InstitutionDropDown.BackColor = System.Drawing.Color.Gainsboro;
            this.InstitutionDropDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InstitutionDropDown.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstitutionDropDown.FormattingEnabled = true;
            this.InstitutionDropDown.Location = new System.Drawing.Point(54, 392);
            this.InstitutionDropDown.Name = "InstitutionDropDown";
            this.InstitutionDropDown.Size = new System.Drawing.Size(208, 24);
            this.InstitutionDropDown.TabIndex = 14;
            this.InstitutionDropDown.SelectedIndexChanged += new System.EventHandler(this.InstitutionDropDown_SelectedIndexChanged);
            // 
            // Schools_Lbl
            // 
            this.Schools_Lbl.AutoSize = true;
            this.Schools_Lbl.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Schools_Lbl.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Schools_Lbl.Location = new System.Drawing.Point(54, 368);
            this.Schools_Lbl.Name = "Schools_Lbl";
            this.Schools_Lbl.Size = new System.Drawing.Size(80, 18);
            this.Schools_Lbl.TabIndex = 15;
            this.Schools_Lbl.Text = "Institutions";
            // 
            // Group_Lbls
            // 
            this.Group_Lbls.AutoSize = true;
            this.Group_Lbls.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Group_Lbls.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Group_Lbls.Location = new System.Drawing.Point(54, 423);
            this.Group_Lbls.Name = "Group_Lbls";
            this.Group_Lbls.Size = new System.Drawing.Size(59, 18);
            this.Group_Lbls.TabIndex = 16;
            this.Group_Lbls.Text = "Groups";
            // 
            // messageTemplateBindingSource
            // 
            this.messageTemplateBindingSource.DataSource = typeof(ILS_Blackboard_Messages.MessageTemplate);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.ClientSize = new System.Drawing.Size(656, 530);
            this.Controls.Add(this.Group_Lbls);
            this.Controls.Add(this.Schools_Lbl);
            this.Controls.Add(this.InstitutionDropDown);
            this.Controls.Add(this.GroupDropdown);
            this.Controls.Add(this.argumentsBtn);
            this.Controls.Add(this.templateID);
            this.Controls.Add(this.TemplateID_label);
            this.Controls.Add(this.TemplateList_Lbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Templates_ListBox);
            this.Controls.Add(this.Message_TextBox);
            this.Controls.Add(this.Submit_Button);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Secret_TextBox);
            this.Controls.Add(this.Key_TextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(672, 569);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Blackboard Messenger Set Up Tool";
            ((System.ComponentModel.ISupportInitialize)(this.messageTemplateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Key_TextBox;
        private System.Windows.Forms.TextBox Secret_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Submit_Button;
        private System.Windows.Forms.TextBox Message_TextBox;
        private System.Windows.Forms.ListBox Templates_ListBox;
        private System.Windows.Forms.BindingSource messageTemplateBindingSource;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label TemplateList_Lbl;
        private System.Windows.Forms.Label TemplateID_label;
        private System.Windows.Forms.Label templateID;
        private System.Windows.Forms.Button argumentsBtn;
        private System.Windows.Forms.ComboBox GroupDropdown;
        private System.Windows.Forms.ComboBox InstitutionDropDown;
        private System.Windows.Forms.Label Schools_Lbl;
        private System.Windows.Forms.Label Group_Lbls;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}