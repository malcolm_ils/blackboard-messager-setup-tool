﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ILS_Blackboard_Messages
{
    public partial class ArgPreviewForm : Form
    {
        public CurrentData data { get; set; }
        public ArgPreviewForm()
        {
            
            InitializeComponent();
        }
        public ArgPreviewForm(CurrentData current)
        {

            InitializeComponent();
            if (current != null)
            {
                data = current;
                user_txtbox.Text = data.Key;
                pass_txtbox.Text = data.Secret;
                templateId_txtbox.Text = data.TemplateID;
                groupId_txtbox.Text = data.GroupID;
                args_txtbox.Text = data.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
